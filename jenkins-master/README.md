# Jenkins Master

## Requirements

- Jenkins Copy Artifact Plugin

- Jenkins Robot Framework Plugin

## Used Job Names

All used projects were Pipeline projects. Used project names were:

- Petclinic_Regular_Test_Run

- Petclinic_Quarantine_Test_Run

- Rebot

Either use these names or change your own job name to Jenkinsfiles.
Currently test runs also use environment variable for job name when creating Petclinic stack, that's why underscores are necessary in job name.

## Rebot Copying Artifacts

Give Rebot permission to copy artifacts from projects Petclinic_Regular_Test_Run and Petclinic_Quarantine_Test_Run.
This can be done from Project's Configurations -> Check Permissions to Copy Artifact and insert project names-

## TODO

Robot Framework Test Runs to its own container!
